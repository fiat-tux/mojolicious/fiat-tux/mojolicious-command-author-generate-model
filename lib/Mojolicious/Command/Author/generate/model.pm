# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Mojolicious::Command::Author::generate::model;
use Mojo::Base 'Mojolicious::Command', -signatures;
use Mojo::Util qw(class_to_file class_to_path decamelize);

has description => 'Generate Mojolicious Fiat Tux model files';
has usage       => sub { shift->extract_usage };

our $VERSION = "0.04";

sub run ($self, $class = 'MyApp', @tables) {

    # Model
    @tables = qw/user/ unless scalar @tables;
    for my $table (@tables) {
        $table         = ucfirst($table);
        my $model      = "${class}::Db::$table";
        my $base_class = "${class}::Db";
        my $path       = class_to_path $model;
        $self->render_to_rel_file('model', "lib/$path", { class => $model, base_class => $base_class, table => lc($table).'s' });
    }
}

1;

=pod

=encoding utf-8

=head1 NAME

Mojolicious::Command::Author::generate::model - Fiat Tux model generator command

=head1 SYNOPSIS

  Usage: APPLICATION generate model [OPTIONS] [NAME]
    mojo generate model
    mojo generate model TestApp
    mojo generate model TestApp user article comment
  Options:
    -h, --help   Show this summary of available options
  Nota bene:
    run it from your application directory

=head1 DESCRIPTION

L<Mojolicious::Command::Author::generate::model> generates model files for L<Mojolicious> applications, with Fiat Tux flavor.

Forked from L<Mojolicious::Command::Author::generate::app>
See L<Mojolicious::Commands/"COMMANDS"> for a list of commands that are available by default.

=head1 ATTRIBUTES

L<Mojolicious::Command::Author::generate::model> inherits all attributes from L<Mojolicious::Command> and implements the
following new ones.

=head2 description

  my $description = $app->description;
  $app            = $app->description('Foo');

Short description of this command, used for the command list.

=head2 usage

  my $usage = $app->usage;
  $app      = $app->usage('Foo');

Usage information for this command, used for the help screen.

=head1 METHODS

L<Mojolicious::Command::Author::generate::model> inherits all methods from L<Mojolicious::Command> and implements the
following new ones.

=head2 run

  $app->run(@ARGV);

Run this command.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Guides>, L<https://mojolicious.org>.

=head1 AUTHOR

Luc Didry E<lt>luc@didry.orgE<gt>

=head1 LICENSE

Copyright (C) Luc Didry.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__DATA__

@@ model
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package <%= $class %>;
use Mojo::Base '<%= $base_class %>', -signatures;

has 'table' => '<%= $table %>';

1;
