# NAME

Mojolicious::Command::Author::generate::model - Fiat Tux model generator command

# SYNOPSIS

    Usage: APPLICATION generate model [OPTIONS] [NAME]
      mojo generate model
      mojo generate model TestApp
      mojo generate model TestApp user article comment
    Options:
      -h, --help   Show this summary of available options
    Nota bene:
      run it from your application directory

# DESCRIPTION

[Mojolicious::Command::Author::generate::model](https://metacpan.org/pod/Mojolicious::Command::Author::generate::model) generates model files for [Mojolicious](https://metacpan.org/pod/Mojolicious) applications, with Fiat Tux flavor.

Forked from [Mojolicious::Command::Author::generate::app](https://metacpan.org/pod/Mojolicious::Command::Author::generate::app)
See ["COMMANDS" in Mojolicious::Commands](https://metacpan.org/pod/Mojolicious::Commands#COMMANDS) for a list of commands that are available by default.

# ATTRIBUTES

[Mojolicious::Command::Author::generate::model](https://metacpan.org/pod/Mojolicious::Command::Author::generate::model) inherits all attributes from [Mojolicious::Command](https://metacpan.org/pod/Mojolicious::Command) and implements the
following new ones.

## description

    my $description = $app->description;
    $app            = $app->description('Foo');

Short description of this command, used for the command list.

## usage

    my $usage = $app->usage;
    $app      = $app->usage('Foo');

Usage information for this command, used for the help screen.

# METHODS

[Mojolicious::Command::Author::generate::model](https://metacpan.org/pod/Mojolicious::Command::Author::generate::model) inherits all methods from [Mojolicious::Command](https://metacpan.org/pod/Mojolicious::Command) and implements the
following new ones.

## run

    $app->run(@ARGV);

Run this command.

# SEE ALSO

[Mojolicious](https://metacpan.org/pod/Mojolicious), [Mojolicious::Guides](https://metacpan.org/pod/Mojolicious::Guides), [https://mojolicious.org](https://mojolicious.org).

# AUTHOR

Luc Didry <luc@didry.org>

# LICENSE

Copyright (C) Luc Didry.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.
