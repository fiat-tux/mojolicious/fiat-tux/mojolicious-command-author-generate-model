use strict;
use Test::More 0.98;

use_ok $_ for qw(
    Mojolicious::Command::Author::generate::model
);

done_testing;

